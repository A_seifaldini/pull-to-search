import React, { useState, useRef } from "react";
import { StyleSheet, View, StatusBar } from "react-native";
import Search from "./components/Search";
import ScrollView from "./components/ScrollView";
import Content from "./components/Content";
import SearchBox from "./components/SearchBox";
import { useMemoOne } from "use-memo-one";
import Animated, {
	Transitioning,
	TransitioningView,
	Transition
} from "react-native-reanimated";

const { Value } = Animated;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: StatusBar.currentHeight
	}
});

const transition = (
	<Transition.Together>
		<Transition.In durationMs={300} type="scale" />
		<Transition.Out durationMs={300} type="scale" />
	</Transition.Together>
);

export default () => {
	const ref = useRef<TransitioningView>(null);
	const [search, setSearch] = useState(false);
	const translateY = useMemoOne(() => new Value(0), []);
	return (
		<Transitioning.View style={styles.container} {...{ transition, ref }}>
			<Search {...{ translateY }} />
			<ScrollView
				onPull={() => {
					if (ref.current) {
						ref.current.animateNextTransition();
					}
					setSearch(true);
				}}
				{...{ translateY }}>
				<Content />
			</ScrollView>
			<SearchBox
				visible={search}
				onRequestClose={() => {
					if (ref.current) {
						ref.current.animateNextTransition();
					}
					setSearch(false);
				}}
			/>
		</Transitioning.View>
	);
};
