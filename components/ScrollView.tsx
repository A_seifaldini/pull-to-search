import React, { ReactNode, memo, useState } from "react";
import { StyleSheet, View } from "react-native";
import { PanGestureHandler, State } from "react-native-gesture-handler";
import Animated from "react-native-reanimated";
import { useMemoOne } from "use-memo-one";
import {
	snapPoint,
	verticalPanGestureHandler as rverticalpanGestureHandler,
	onGestureEvent
} from "react-native-redash";
import { THRESHOLD } from "./Search";

const {
	Value,
	Clock,
	eq,
	startClock,
	set,
	add,
	and,
	greaterOrEq,
	lessOrEq,
	cond,
	decay,
	block,
	not,
	spring,
	abs,
	multiply,
	divide,
	sub,
	useCode,
	call,
	neq,
	pow,
	SpringUtils,
	diff,
	min
} = Animated;

const friction = (ratio: Animated.Node<number>) =>
	multiply(0.52, pow(sub(1, ratio), 2));

const verticalPanGestureHandler = () => {
	const translationY = new Value(0);
	const velocityY = new Value(0);
	const state = new Value(State.UNDETERMINED);
	const gestureHandler = onGestureEvent({
		translationY,
		velocityY,
		state
	});
	return {
		translationY,
		state,
		velocityY,
		gestureHandler
	};
};

interface withScrollProps {
	translationY: Animated.Value<number>;
	velocityY: Animated.Value<number>;
	state: Animated.Value<State>;
	contentHeight: number;
	containerHeight: number;
}

const withScroll = ({
	translationY,
	velocityY,
	state: gestureState,
	contentHeight,
	containerHeight
}: withScrollPro) => {
	const clock = new Clock();
	const delta = new Value(0);
	const state = {
		time: new Value(0),
		position: new Value(0),
		velocity: new Value(0),
		finished: new Value(0)
	};
	const config = SpringUtils.makeDefaultConfig();
	const upperBound = 0;
	const lowerBound = -1 * (contentHeight - containerHeight);
	const isInBound = and(
		lessOrEq(state.position, upperBound),
		greaterOrEq(state.position, lowerBound)
	);
	const isSpringing = new Value(0);
	const overScroll = sub(
		state.position,
		cond(greaterOrEq(state.position, 0), upperBound, lowerBound)
	);
	return block([
		startClock(clock),
		set(delta, diff(translationY)),
		cond(
			eq(gestureState, State.ACTIVE),
			[
				set(isSpringing, 0),
				set(
					state.position,
					add(
						state.position,
						cond(
							isInBound,
							delta,
							multiply(
								delta,
								friction(min(divide(abs(overScroll), containerHeight), 1))
							)
						)
					)
				),
				set(state.velocity, velocityY),
				set(state.time, 0)
			],
			[
				set(translationY, 0),
				cond(
					and(isInBound, not(isSpringing)),
					[decay(clock, state, { deceleration: 0.997 })],
					[
						set(isSpringing, 1),
						set(
							config.toValue,
							snapPoint(state.position, state.velocity, [
								lowerBound,
								upperBound
							])
						),
						spring(clock, state, config)
					]
				)
			]
		),
		state.position
	]);
};

const styles = StyleSheet.create({
	container: {
		flex: 1
	}
});

interface ScrollViewProps {
	children: ReactNode;
	translateY: Animated.Value<number>;
	onPull: () => void;
}

export default memo(({ children, translateY, onPull }: ScrollViewProps) => {
	const [containerHeight, setContainerHeight] = useState(0);
	const [contentHeight, setContentHeight] = useState(0);
	const { gestureHandler, translationY, velocityY, state } = useMemoOne(
		() => verticalPanGestureHandler(),
		[]
	);
	useCode(
		block([
			set(
				translateY,
				withScroll({
					translationY,
					velocityY,
					state,
					contentHeight,
					containerHeight
				})
			),
			cond(
				and(greaterOrEq(translateY, THRESHOLD), neq(state, State.ACTIVE)),
				call([], onPull)
			),
			[]
		]),
		[containerHeight,contentHeight,onPull]
	);
	return (
		<View
			style={styles.container}
			onLayout={({
				nativeEvent: {
					layout: { height }
				}
			}) => setContainerHeight(height)}>
			<PanGestureHandler {...gestureHandler}>
				<Animated.View
					onLayout={({
						nativeEvent: {
							layout: { height }
						}
					}) => setContentHeight(height)}
					style={{ transform: [{ translateY }] }}>
					{children}
				</Animated.View>
			</PanGestureHandler>
		</View>
	);
});
